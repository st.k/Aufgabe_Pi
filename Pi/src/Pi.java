/**
 * Mithilfe des Verhältnisses zwischen einem Quadrat der Länge 1 und einem darin
 * enthaltenen Einheitskreis wird mit der Probalistischen Bestimmung die Zahl Pi angenähert.
 * @author Stephanie Kaswurm, Anna-Lena Klaus
 *
 */
public class Pi {
   
     public static void main(String[] args) {
         System.out.println(berechnenPi(10000000));
     }
     // Den Wert Pi berechnen, wobei umso höher die Genauigkeit, umso näher ist die Zahl an Pi.
    public static double berechnenPi (int genauigkeit) { //double da es wichtig ist, dass mehrere Kommastellen möglich sind
         double punkteImKreis = 0; // Punkte im Kreis
         double punkteGesamt = genauigkeit; // Punkte, die insgesamt getestet wurden
      for(; genauigkeit >0; genauigkeit--) {
            double zufallszahlX = Math.random(); // X Koordinate
            double zufallszahlY = Math.random(); // Y Koordinate
   
            //Prüfen ob der Punkt im Kreis liegt oder nicht.
             if(Math.pow(zufallszahlX, 2.0) + Math.pow(zufallszahlY, 2.0) <= 1.0) {
                 punkteImKreis++;
             }
      }

        // Fläche unter dem Einheitskreis = (Punkte im Kreis/Gesamtpunkte)
        // Fläche Einheitskreises = 1/4 * Gesamtfläche --> Alles wird * 4 gerechnet
        return (punkteImKreis / punkteGesamt) * 4;      }
 }
//0.15707963268 Diese Abweichung ist erlaubt = 5%